# OpenMentOS - Media & Entertainment OS

This goal of this project for me was to learn how to build a custom linux
distibution using the Yocto project.

My custom distribution is a Media Center based on Kodi designed to run on
variaty of platforms, initially the Raspberry Pi 3B+.

## Project Goals
To build a custom linux distribution (hereafter OpenMentOS) that will include:
1. `Kodi` - The latest version with all the basic functionality.
2. SSH Server with public key authentication, where the key-pair is generated
at build-time.
3. samba client and server
4. nfs client and server
5. WD for soft reset in case Kodi freezes (I had to manually unplug-plug my RPi
countless times and I had enough)
6. Python 3.8
7. Different partitions for the rootfs and user-data such that upgrading the
system won't cause data-loss

The project is based on Poky version `dunfell`

## Getting Started
### System Requirements
First, follow the instructions under Yocto's [System Requirements document](https://www.yoctoproject.org/docs/2.0/ref-manual/ref-manual.html#intro-requirements),
and install the _Essentials_ packages for your build host machine distribution.

#### Install Java
In addition, you'll have to install `java` on your machine in order to build
`Kodi`.
For some weird reason the current recipe assumes hard-coded `/usr/bin/java`.
This will be fixed in future version.

On Ubuntu-based machine this is done by
```sh
sudo apt install jre-default
```

### Initialize submodules
```sh
git submodule update --init --recursive
```

### Setup your enviroment
Source the init-build-env script:
```
source om-init-build-env [BUILD_DIR]
```
`BUILD_DIR` is optional. It defaults to `build/` under the current working dir.

### Build
```sh
bitbake openmmentos-image
```
