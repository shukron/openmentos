#!/bin/sh

if [ -n "$BASH_SOURCE" ]; then
    THIS_SCRIPT=$BASH_SOURCE
elif [ -n "$ZSH_NAME" ]; then
    THIS_SCRIPT=$0
else
    THIS_SCRIPT="$(pwd)/om-init-build-env"
    if [ ! -e "$THIS_SCRIPT" ]; then
        echo "Error: $THIS_SCRIPT doesn't exist!" >&2
        echo "Please run this script in om-init-build-env's directory." >&2
        return 1
    fi
fi

if [ -z "$OMROOT" ]; then
    OMROOT=$(dirname "$THIS_SCRIPT")
    OMROOT=$(readlink -f "$OMROOT")
fi

if [ ! -e "$OMROOT/poky/oe-init-build-env" ]; then
    echo "Error: cound not find poky/oe-init-build-env"
    echo "  Did you forget to run 'git submodule update --init --recursive'?"
    return 1
fi

TEMPLATECONF=$OMROOT/meta-openmentos/conf . "$OMROOT/poky/oe-init-build-env" $@